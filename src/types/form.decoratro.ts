import { string, StringSchema, object, ObjectSchema } from 'yup';

export const Form = (data?: StringSchema<string>) => (target: any, propertyKey: string) => FormValidation.registerProp(target, propertyKey, data);

interface Prop {
  name: string;
  validator: StringSchema<string | undefined>;
}

interface FormError {
  field: string;
  description: string;
}

export class FormValidation {
  private static schemaMap: Map<any, Prop[]> = new Map();

  static registerProp(target: any, propertyKey: string, data?: StringSchema<string>): void {
    const keyName = target.constructor.name
    let keys = this.schemaMap.get(keyName);
    if (!keys) {
      keys = [];
      this.schemaMap.set(keyName, keys);
    }
    keys.push({ name: propertyKey, validator: data ?? string() });
  }

  static getSchema(metatype: any): ObjectSchema<object | undefined> | undefined {
    const schemaProps = this.schemaMap.get(metatype.name);
    if (!schemaProps) {
      return undefined;
    }
    return object().shape(schemaProps.reduce((res, prop) => ({ ...res, [prop.name]: prop.validator }), {}))
  }

  static getFormSchema(metatype: any) {
    const schemaProps = this.schemaMap.get(metatype.name);
    return schemaProps?.map(prop => ({ name: prop.name, type: 'string' }))
  }
}
