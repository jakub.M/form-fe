import { Form } from './form.decoratro';
import { string } from 'yup';

export class AppDto {
  @Form(string().required())
  email: string;
}
