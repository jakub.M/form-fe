import { useState } from 'react';
import axios, { Method } from 'axios';
import { includes } from 'lodash';
import { useForm } from 'react-hook-form';
import cogoToast from 'cogo-toast';
import { MsgResutl } from './message-result';

const apiUrl = 'http://localhost:3001/';

interface ApiProps<T> {
  method: Method;
  path: string;
  msgResult?: MsgResutl;
  data?: T;
  onSuccess?: Function;
}
interface useApiProps {
  validationSchema?: any;
  defaultValues?: any;
}
export const useApi = (props?: useApiProps) => {
  const [loading, setLoading] = useState(false);
  const methods = useForm({ ...props });

  const api = async <T>({
    method,
    path,
    msgResult,
    data,
    onSuccess,
  }: ApiProps<T>) => {
    cogoToast.loading('This is a loading message');
    setLoading(true);
    await axios({
      method,
      url: apiUrl + path,
      data,
      timeout: 4000,
      withCredentials: true,
    })
      .then((res) => {
        if (includes([MsgResutl.Both, MsgResutl.Success], msgResult)) {
          cogoToast.success('This is a success message');
        }
        onSuccess?.(res);
      })
      .catch((err) => {
        if (includes([MsgResutl.Both, MsgResutl.Failure], msgResult)) {
            cogoToast.error(`What's wrogn :(`); // here handlign err.response?.data?.message?.description as []
        }
      });
    setLoading(false);
  };

  return { api, loading, methods };
};
