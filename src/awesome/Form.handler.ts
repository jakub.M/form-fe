import cogoToast from 'cogo-toast';
import { useApi } from './useApi';
import { FormValidation } from '../types/form.decoratro';
import { AppDto } from '../types/AppDto';

export const MyFormHanlder = () => {
  const { api, methods, loading } = useApi({ validationSchema: FormValidation.getSchema(AppDto) });

  const onSubmit = methods.handleSubmit((data: any) => {
    api(<any>{
      method: 'Post',
      path: '',
      data,
      onSuccess: () => {
        cogoToast.success('This is a success message');
      },
    });
  });

  return { methods, onSubmit, loading, metatype: AppDto };
};
