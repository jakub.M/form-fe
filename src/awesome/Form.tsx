import React from 'react'
import { FormContext } from 'react-hook-form'
import { Button } from '@material-ui/core'
import { FormValidation } from '../types/form.decoratro'
import { ItfFormInput } from './input'
import { startCase } from 'lodash';

export const ItfForm = ({ handler, children }: any) => {
  const { methods, onSubmit, metatype } = handler()

  const context = children ?? FormValidation.getFormSchema(metatype)?.map(field => {
    if (field.type === 'string') {
      return <ItfFormInput {...{ name: field.name, label: startCase(field.name) }} />
    }
  });

  return (
    <FormContext {...methods}>
      <form onSubmit={onSubmit} >
        {context}
        <Button type='submit'  variant="contained" color="primary">Submit</Button>
      </form>
    </FormContext>
  )
}