import React from 'react';
import { ItfForm } from './awesome/Form';
import { ItfFormInput } from './awesome/input';
import { MyFormHanlder } from './awesome/Form.handler';
import { AppDto } from './types/AppDto';


// ItfForm can by simple form generator (e.g. when children is null), has fileds from 'AppDto' 
export const App = () =>
<>
<ItfForm {...{ handler: MyFormHanlder }}>
  <ItfFormInput {...{ name: 'email', label: 'email' }} />
</ItfForm>
<ItfForm {...{ handler: MyFormHanlder }} />
</>
